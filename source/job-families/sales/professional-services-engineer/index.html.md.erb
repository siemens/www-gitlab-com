---
layout: job_family_page
title: "Professional Services Engineer"
---

Professional Services Engineers provide professional services on-site or remote deployment of GitLab technology and solutions as well as training. The Professional Services Engineer will act as the technical representative leading the direct interaction with the customer’s personnel and project teams by rolling out best practices. This role helps the customer to faster benefit from the GitLab solution and realize business value. Meanwhile, Professional Services Engineers also need to provide feedback to product management and engineering team on the actual field experience through customer engagements and implementations.

To learn more, see the [Professional Services Engineer handbook](/handbook/customer-success/professional-services-engineering)

## Level Junior

Junior Professional Services Engineers share the same requirements and responsibilities outlined on our jobs page but typically join with less relevant experience, alternate background and/or less industry exposure than a typical Professional Services Engineer.`

### Job Grade 

The Junior Professional Services Engineer is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

- Represent GitLab and its values in public communication around broader initiatives, specific projects, and community contributions.
- Install & configure GitLab solutions in customer environment as per Statement of Work (SOW)
- Provide technical training sessions remotely and/or on-site
- Develop and implement migration plan for customer VCS & data migration
- Assist GitLab customer support team to diagnose and troubleshoot support cases when necessary
- Develop & maintain custom scripts for integration or policy to align with custom requirements
- Create detailed documentation for implementation, guides and training.
- Support Sales on quoting PS and drafting SOW to respond PS requests
- Travel required - 40%
- Managing creation of new and maintaining of existing training content

<%= partial("job-families/sales/performance", :locals => { :roleName => "Professional Services Engineer" }) %>

## Level Intermediate

The Intermediate Professional Services Engineer role extends the Junior Professional Services Engineer role.

### Job Grade 

The Professional Services Engineer is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

- Experienced with and proven knowledge with major cloud technologies and public cloud providers
- Experience with multiple highly available architectures and concepts. Kubernetes operational and deployment experience.
- Solve technical problems of high scope and complexity.
- Help to define and improve our internal standards for style, maintainability, and best practices for customer private/public cloud environments.

## Level Senior

The Senior Professional Services Engineer role extends the Intermediate Professional Services Engineer role.

### Job Grade 

The Senior Professional Services Engineer is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

- Mastery with and proven knowledge with major cloud technologies and public cloud providers
- Mastery with multiple highly available architectures and concepts. Kubernetes operational and deployment experience.
- Exert influence on the overall objectives and long-range goals of your team.
- Provide mentorship for Junior and Intermediate Professional Services Engineers to help them grow in their technical responsibilities and remove blockers to their autonomy.

### Requirements

- Deep knowledge of software development lifecycle and development pipeline
- Understanding of continuous integration, continuous deployment, ChatOps, and cloud native
- Above average knowledge of Unix and Unix based Operating Systems
  - Installation and operation of Linux operating systems and hardware investigation/manipulation commands
  - BASH/Shell scripting including systems and init.d startup scripts
  - Package management (RPM, etc. to add/remove/update packages)
  - Understanding of system log files/logging infrastructure
- B.Sc. in Computer Science or equivalent experience
- Programming/scripting experience & skill is required (Bash, Ruby, and Python)
- Project management experience & skills
- SCM admin and/or PS experience would be a plus
- Set up HA/DR, working with Containers and Schedulers (Kubernetes preferred) and also experience with AWS stack (EC2, ECS, RDS, ElastiCache)
- Experience with Ruby on Rails applications and Git
- Ability to use GitLab

## Specialties

Read more about what a [specialty](/handbook/hiring/#definitions) is at GitLab.

### Principal Technical Architect

#### Job Grade 

The Principal Technical Architect is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities 
* Responsible for the application design, development and support of GitLab related Professional Services projects. 
* Demonstrate progressive leadership in the full life cycle of the software development environment. 
* Use of design documentation and best-practice methodologies to be responsible for the detailed design of Gitlab related projects from inception through production support. 
* Ensuring that the system accurately meets the defined expectations of the business unit, ensuring that proper testing is implemented and performance requirements are closely monitored by working with the development teams.
* Knows and applies fundamental concepts, practices, and procedures of particular field of specialization, with awareness of related fields.
* Coordinate and oversee functional implementation activities for internal and partner consultants, developers, and customer points of contact (POC) on large scale enterprise projects
* Mentor PS and Partner Consultants providing guidance regarding ‘Best Practice,’ communication, and implementation strategies  
* Liaise with and support of other functional groups within GitLab – including (but not limited to) training, development, support, product, engineering
* Provide business use cases supporting product QA of new releases and short- and long-term objectives of the product roadmap
* Review and provide input to PS training materials and presentations
* Develop case studies, presentations, and internal process development
* Provide support during statements of work (SOW) development for add-on or extended functionality by PS team

#### Requirements 
* Bachelors Degree in Information Technology, Computer Science or other advanced technical degree
* Strong written and verbal communications
* 5-10 years experience delivering consulting services
* 5-10 years of enterprise level software development experience
* 5-8 years DevOps Platform experience
* Strong computer skills. Proficiency in Word, Excel and Power Point required. A working knowledge of core business systems preferred
* Excellent written and verbal communication skills with the ability to focus and clarify concepts
* Demonstrated problem solving and decision-making abilities with effective organizational and time management skills; the ability to handle multiple projects and priorities effectively in a fast-paced environment with minimal supervision
* Strong organizational, multi-tasking and presentation skills. Ability to create momentum and foster organizational change
* You share our values, and work in accordance with those values.
* [Leadership at GitLab](/company/team/structure/#director-group)
* Ability to use GitLab

### Professional Services Technical Instructor

#### Job Grade

The Professional Services Technical Instructor is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities
* Deliver live and asynchronous experiential instructional sessions that support adult learning best practices, address needs identified in a job/task analysis, drive behavior change, and improve the performance of GitLab customers and partners.
* Deliver excellent customer-service during training events including arriving and running training sessions punctually and professionally.
* Review customer feedback and collaborate with team members to continually improve the customer learning experience.
* Respond professionally and attentively to training session attendees to ensure they are satisfied with their learning experience.
* Collaborate cross-functionally with team members to plan and develop slides and labs for instructional sessions.
* Stay up to date with the latest GitLab feature releases and incorporate them into training deliveries.
* Serve as a subject matter expert for certification program design and assessment development.
* Meet all milestones and final deliverables by deadlines; when delays do occur, work with the team to estimate, monitor, adjust, and proactively communicate revised deadlines as needed

#### Requirements
* Technical degree such as B.S. or M.S. or relevant work experience
* 5+ years experience delivering live technical training and labs on how to use SaaS development solutions 
* Proficient with commonly-used coding languages and tools such as Python, Java, Docker, Kubernetes, YAML, Git, and Visual Studio
* Working knowledge of and experience with using GitLab throughout the concurrent devops life cycle
* Dynamic, engaging presentation style with strong presentation skills specifically in delivering complex technical training to a variety of audience types
* Passion for helping customers succeed and for exceeding customer expectations
* Knowledge of and experience with applying adult learning methods and practices
* Strong oral and written communication skills and diverse experience working with internal customers and stakeholders in both live and virtual environments
* Proven ability to clearly articulate complex concepts in simple terms
* Excellent team player
* Detail-oriented with strong project management skills and requiring minimal supervision
* Experience with virtual meeting/webinar tools such as Zoom
* Ability to travel 50% or more both within the United States and globally
* Experience working with a variety of Learning Management and Learning Experience Systems and various virtual meeting management tools such as Zoom is a plus
* Familiarity with statically generated websites are preferred, as we take a handbook-first approach to Sales and Customer Enablement
* Knowledge of the software development life cycle, DevOps, and/or open source software is preferred
* Ability to use GitLab

### Public Sector

#### Additional Requirements

- TS/SCI Security Clearance
- Knowledge of and at least 4 years of experience with Public Sector customers

### Embedded on-site engineer

#### Additional Responsibilities

- Embed on site with a customer for a period of 3, 6, or 12 months at a time
- Act as a GitLab evangelist onsite with customer's teams
- Identify pain points and issues with planning, development, test, compliance, security, build and orchestration processes.
- Assist GitLab customer support team to diagnose and troubleshoot support cases when necessary
- Help review processes and identify areas for improvement including the use of automation

#### Additional Requirements

- Presentation skills with a high degree of comfort speaking with executives, IT Management, and developers
- Strong verbal and written communication skills
- High level of comfort communicating effectively across customer teams, groups or divisions
- Ideal candidates will preferably have 7 plus years IT industry or IT technical sales related experience
- Experience with modern development practices strongly preferred (Kubernetes, Docker, Linux, DevOps Pipelines (CI/CD), Application Security, Cloud providers)
- Experience with several of the following tools strongly preferred:
  - Ruby on Rails, Java, .Net, Python
  - Git, Bitbucket, GitHub, SVN
  - Jira, VersionOne, TFS
  - Jenkins, CircleCI
  - Veracode, Checkmarx, Black Duck
  - Artifactory, Nexus
  - New Relic, Nagios
  - mono-repo and distributed-repo approaches
  - BASH / Shell scripting

## Senior Manager, Professional Services

### Job Grade 

The Senior Manager, Professional Services is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities 
* Lead delivery of consulting, education and implementation services to our customers, overseeing resource management, project delivery and operational success metrics.
* Be accountable for services financial goals and metrics, including revenue, gross margin and utilization.
* Own operational metrics, such as time-to-value, customer satisfaction and on-time completion of projects.

### Requirements
* 5+ years of experience managing, leading and/or delivering professional services or customer success
* 3+ years experience with a subscription-based business model, delivering on-premises and SaaS solutions
* Experience managing technical, cross-functional professional services teams (e.g., consulting, implementation, trainers, project managers) and delivery partners

## Manager, Professional Services

### Job Grade 

The Manager, Professional Services is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

- Work with the Customer Success Director to help establish and manage goals and responsibilities for Professional Services Engineers
- Assist in development of thought leadership, event-specific, and customer-facing presentations
- Share hands-on technical preparation and presentation work for key accounts helping sell on the value of what GitLab professional services has to offer
- Ensure the Professional Services Engineers exceeds corporate expectations in core knowledge, communication, and execution
- Define and maintain a high bar for team member expectations and enable the team to achieve it
- Challenge the team and yourself to continually learn and grow as trusted advisors to clients
- Monitor performance of team members and provide timely feedback and development assistance
- Create, review, and approve formal statements of work, change requests, and proposals
- Prepare weekly revenue forecast worksheet and create action plans to address issues
- Manage resource assignments and staffing levels, including recruitment as needed
- Identify and implement improvements to the processes and tools used as a seasoned with experience leading teams of project managers and consultants in support of external customers
- Work together with our sales organization to propose, scope, and price professional services Statements of Work, including managing a PS sales overlay team
- Bring Professional Services team together with Solutions Architects and Technical Account Managers to plan and execute internal projects, ensure that teams have appropriate training and manage resources to deliver GitLab service offerings
- Provide leadership and guidance to coach, motivate and lead services team members to their optimum performance levels and career development
- Ensure delivery model is focused on quality, cost effective delivery of services, and customer success outcomes
- Remains knowledgeable and up-to-date on GitLab releases
- Documents services provided to customers, including new code, techniques, and processes, in such a way as to make such services more efficient in the future and to add to the GitLab community
- Works with the Product Engineering and Support teams, to contribute documentation for GitLab
- Help build programs that the TAMs and PSEs will execute to effectively grow our enterprise customers

### Requirements

All requirements for the Professional Services Engineer role apply to the Manager, Professional Services role, as the management role will need to understand and participate with the day-to-day aspects of the Professional Services Engineer role in addition to managing the team. Additional requirements include:

- Experienced in and with advanced insight to the GitLab platform
- Experienced in giving and received positive and constructive feedback
- Able to adapt to environmental change and retrospecting on success and failures
- Previous leadership experience is a plus
- Experienced in collaborating with other managers and executing strategies
- Proven track record in software/technology sales or consulting and management
- Demonstrated ability to think strategically about business, products, and technical challenges

## Career Ladder

The next step for both individual contributors and managers of people is to move to the [Director of Professional Services](/job-families/sales/director-of-professional-services) job family.

## Performance Indicators
* [Billable utilization](/handbook/customer-success/professional-services-engineering/#implementation-plan) > 70%

## Hiring Process

Candidates can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team).

- Qualified candidates for the Professional Services Engineer will receive a short questionnaire
- Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with a member of our Recruiting team
- Next, candidates will be invited to schedule a first interview with our Director of Customer Success
- For the Professional Services Engineer role, candidates will be invited to schedule an interview with a Customer Success peer and may be asked to participate in a demo of a live install of GitLab
- For the Public Sector Professional Services Engineer role, candidates will be invited to schedule interviews with members of the Customer Success team and our Public Sector Regional Sales Director
- For a manager role, candidates will be invited to schedule interviews with our Regional Sales Directors for North America
- Candidates may be invited to schedule an interview with our CRO
- Finally, candidates may be asked to interview with the CEO

Additional details about our process can be found on our [hiring page](/handbook/hiring/interviewing/).
