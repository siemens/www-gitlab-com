---
layout: markdown_page
title: "Organizational Structure"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Organizational chart

You can see who reports to whom on our [organizational chart](/company/team/org-chart).

## Layers

| Level | Example(s) | Peer group / shorthand for peer group |
|--------------------------------|---------------------------------------|-------------------------------------|
| Board member | [Chief Executive Officer](/job-families/chief-executive-officer/chief-executive-officer) | Board |
| Executive | [Chief People Officer](/job-families/people-ops/chief-people-officer/) and [EVP of Engineering](/job-families/engineering/engineering-management/#executive-vp-of-engineering) | Executives / [E-group](/company/team/structure/#e-group) |
| Senior Leader | Senior Director or [VP of Global Channels](/job-families/sales/vp-of-global-channels/) | Senior leaders / [S-group](/company/team/structure/#s-group) |
| Director | [Director of Engineering](/job-families/engineering/backend-engineer/#director-of-engineering) | Directors / [D-group](/company/team/structure/#director-group) |
| Manager | [Engineering Manager](/job-families/engineering/backend-engineer/#engineering-manager) | Managers / [M-group](/company/team/structure/#management-group) |
| Individual contributor (IC) | [Staff Developer](/job-families/engineering/developer/#staff-developer) | ICs |

GitLab Inc. has at most six layers in the company structure (IC, Manager, Director, Senior Leadership, Executives, Board).
You can skip layers but you can never have someone reporting to the same layer since that creates too many layers in the organization.
The CEO is the only person who is part of two levels: the board and the executives.

## Levels

### Individual Contributor

Individual contributors are [Managers of One](/handbook/values/#managers-of-one).
Along with the [E-group](/company/team/structure/#e-group), [S-group](/company/team/structure/#s-group), [Director Group](/company/team/structure/#director-group), and [Management Group](/company/team/structure/#management-group), ICs make up the [GitLab team members](/handbook/leadership/#gitlab-team-members).

### Manager

Managers are people managers.
They belong to the [Management Group](/company/team/structure/#management-group).

#### Management group

Members of the management group are expected to demonstrate leadership in the way all [GitLab team members are](/handbook/leadership/#gitlab-team-members), plus:

1. Ensuring team members feel included and valued is one of the most important tasks of a manager. Proactively create psychological safety with your team members so that diverse perspectives can be heard and everyone can communicate and contribute authentically and creatively.
1. Ensuring team members understand what is expected of them in their roles is a critical role that managers have to ensure company success. Managers should ensure job families include specific [performance indicators](/handbook/hiring/job-families/#why-job-families-have-performance-indicators) and that is clearly communicated to each team member.
1. [Managing underperformance](/handbook/underperformance) is another important task of a manager.
1. When times are great, be a voice of moderation. When times are bad, be a voice of hope. In order to effectively be the voice of hope, managers should make sure they understand the company mission, goals, and leadership decisions. If managers have concerns about leadership decisions, they should voice them to leaders in order to understand the context, share key insights from their team members to ensure we make sound decisions, and then be able to explain context to their team members.
1. To maintain an effective organization, a manager's span of control should be around 7 people, ranging anywhere from 4 to 10. Below this range the inefficiency of an extra organizational layer is larger than the benefit of a specialized group. Above this range, the manager doesn't have time to do proper 1:1s anymore.
1. The span of control is the same at every level of the organization. This prevents the organization from having an additional layer that adds cost and reduces decision making speed. To increase the number of reports you can handle increase delegation to the [directly responsible individual](/handbook/people-group/directly-responsible-individuals/) who can be a manager. Size the number of reports on where you expect the organization to be in 6 months from now.
1. When you praise someone, try to do it [publicly and in front of an audience](/handbook/communication/#say-thanks). When you give suggestions to improve, do it privately [1 on 1](/handbook/leadership/1-1/). A [Harvard study](https://hbr.org/2013/03/the-ideal-praise-to-criticism) has shown that the ideal ratio of positive to negative feedback for high-performing teams is nearly 6:1. Be generous with your positive feedback.
1. Express gratitude in team meetings, group conversations, and other communications to people who constructively challenge your opinions and ask difficult questions. This helps create psychological safety, promote our values, and prevent [the five dysfunctions](/handbook/values/#five-dysfunctions).
1. Understand that there are different ways to get to the same goal. There are different perspectives, and discussions need to happen.
1. When someone says they are considering quitting, drop everything and listen to them. Ask questions to find out what their concerns are. If you delay, the person will not feel valued and the decision will be irreversible.
1. In addition to announcing new team member arrivals, departures are also announced in the `#team-member-updates` chat channel (but only after the Google/Slack accounts are revoked, see the [offboarding page](/handbook/people-group/offboarding/) and the [offboarding checklist](https://gitlab.com/gitlab-com/people-group/employment-templates-2/blob/master/.gitlab/issue_templates/offboarding.md) for details). We must respect the privacy of the individual concerned. If you are asked why someone has left or is leaving, please refer that person to the [general guidelines](/handbook/communication/#not-public) section of the handbook where we describe what can and cannot be shared.
1. People should not be given a raise or a title because they ask for it or threaten to quit. We should pro-actively give raises and promote people without people asking. If you do it when people ask, you are being unfair to people that don't ask and you'll end up with many more people asking.
1. Don't refer to GitLab [as a family](https://hbr.org/2014/06/your-company-is-not-a-family). It is great that our team feels like a close-knit group and we should encourage that, as this builds a stronger team. But _families_ and _teams_ are different. _Families_ come together for the relationship and do what is critical to retain it. _Teams_ are assembled for the task and do what is required to complete it. Don't put the relationship above the task. Besides, families don't have an [offboarding process](/handbook/people-group/offboarding/). Families should have unconditional love, while teams have conditional love. [The best companies are supporters of families.](https://twitter.com/myriadwill/status/917772249624702976). The idea that your team is your family can lead to unwarranted pressure: ["this office is like a family... in that there is vague pressure to be here on holidays"](https://twitter.com/emilieschario/status/1182454709925416961)
1. Praise and credit the work of your reports to the rest of the company, never present it as your own. This and many other great lessons can be found in an [Ask MetaFilter thread](http://ask.metafilter.com/300002/My-best-manager-did-this) worth reading.
1. Try to be aware of your [cognitive biases](https://betterhumans.coach.me/cognitive-bias-cheat-sheet-55a472476b18).
1. Combine [consistency and agility](https://hbr.org/2017/01/the-best-strategic-leaders-balance-agility-and-consistency).
1. Consider maintaining a [README](/handbook/engineering/readmes/eric-johnson/) file for your teams which documents your expectations and working style. Encourage MRs to it.
1. Although our time scales are shorter, there is a great article about [how to think about PIPs](https://mfbt.ca/how-i-talk-to-leaders-about-firing-people-8149dfcb035b).
1. Do everything to unblock people. If someone has a question that is keeping them from being productive, try to answer the question yourself or find someone who can.
1. Communicate in a [professional manner](/company/culture/all-remote/effective-communication/) as though your words will be shared widely (e.g. published in a newspaper).
1. Employ [multimodal communication](/handbook/communication/#multimodal-communication) to broadcast important decisions.
1. You are expected to [respond on social media](/handbook/marketing/social-media-guidelines/). If you have questions, please seek counsel in the `#social_media` chat channel.
1. Make sure your reports experience a [sense of progress](http://tomtunguz.com/progress-principle/) in their work.
1. A tweet by [Sam Altman](https://twitter.com/sama/status/804138512878473220) combined with a reply by [Paul Graham](https://twitter.com/paulg/status/804209365305659392) says it best: "People either get shit done or they don't. And it's easy to be tricked because they can be smart but never actually do anything." Watch for results instead of articulate answers to questions, otherwise you'll take too much time identifying under-performers.
1. [GitLab Contribute](/company/culture/contribute/) (formerly GitLab summits) is meant for [informal communication](/company/culture/all-remote/informal-communication/) and [bonding](/company/culture/all-remote/in-person/) across the company. There is a limited time for business activities during GitLab Contribute so all our regular meetings should happen outside of it. We want informal, cross team, and open-ended meetings that include individual contributors. For example, inviting everyone to suggest currently missing functionality in GitLab.
1. Never delay a decision until GitLab Contribute. Instead, use them as a deadline to get things done earlier.
1. We don't have explicit 20% time at GitLab. We measure [results and not hours](/handbook/values/#measure-results-not-hours). If people are getting good results in the work that is assigned to them they are free to contribute to other parts of the company or work on a pet project. Don't say, "Your work on that pet project is hurting your performance." Instead, say, "We agreed to get X done but it is delayed, what happened and how can I help?"
1. Pick a metric before launching something new. 9 out of 10 launches fail. If a project is not working out shut it down completely. Starving a team of headcount to have it die a slow death is not frugal nor motivating. Fund the winners which will still take years to break even.
1. Do not discuss raises in advance because the salary calculator may change before the amount of the raise is decided.
2. Do not discuss promotions, salary increases, or bonuses until the changes have gone through the entire approval process. People Ops will inform the manager when they can inform the team member of the promotion and increase.
1. Instead of prescribing a direction to your reports, it is best to ask questions following the [Socratic method](https://en.wikipedia.org/wiki/Socratic_method) until you're happy with the direction. Your reports will have deeper knowledge in a more narrow area, so it is easy to draw different conclusions because they base theirs on different data. That is why the questions are so important.
1. Follow [Berkshire's common injunction](https://www.hb.org/the-psychology-of-human-misjudgment-by-charles-t-munger/): "Always tell us the bad news promptly. It is only the good news that can wait." Make sure to inform your manager of bad news as quickly as possible. Promptly reporting bad news is essential to preserving the trust that is needed to recover from it.
1. Try to avoid military analogies and imagery. We're not an army, we're not at war, there is no battle, we're not killing anyone, and we don't have weapons. Military language is [not inclusive](https://www.london.edu/faculty-and-research/lbsr/killing-the-competition) and can lead to zero sum thinking. We take competing and winning very seriously, but there is no need to describe things using vocabulary of physical violence. Similarly, non-collaborative and aggressive terms like "rockstar" and "badass" put up walls between people. If a term is standard in the industry, for example [killing a Unix process](https://shapeshed.com/unix-kill/#how-to-kill-a-process), it is acceptable to use it because that is more efficient. Do use "primary-secondary" instead of "master-slave" for replication mechanisms.
1. Complain up and explain down. Raise concerns you hear to your manager. When peers or reports complain, explain why a decision was made. If you don't understand why, ask your manager.
1. Create empathy for decisions from other leaders that negatively impact your team by explaining the reasons behind them. Organize a recorded AMA session with your team and the other leaders and encourage your team (as well as yourself) to ask any unanswered questions. Lead by example by ensuring that the discussion exposes what is [best for the organization as a whole](/handbook/values/#global-optimization). Never present yourself as protecting the team from the rest of the organization; this creates a [siege mentality](https://en.wikipedia.org/wiki/Siege_mentality) and hinders [collaboration](/handbook/values/#collaboration).
1. Coach team members to establish good [remote work practices](/company/culture/all-remote/), by encouraging [asynchronous communication](/company/culture/all-remote/management/#asynchronous), being [handbook-first](/handbook/handbook-usage/#why-handbook-first), communicating poor audio or [video quality](/blog/2019/08/05/tips-for-mastering-video-calls/), or pointing out an open microphone.

### Director

Directors are managers of managers.
They make up the [Director Group](/company/team/structure/#director-group).

#### Director group

In some [cost center](/handbook/finance/financial-planning-and-analysis/#cost--reporting-structure), Directors map to departments.
For example, under the G&A Division, Business Operations is a department led by a Director.
This is not a hard-and-fast rule, though, as under G&A, People is all under one department.

Members of the director group are expected to demonstrate leadership in the way all members of the [management group](/company/team/structure/#management-group) are, plus:

1. They have the ability to align the day-to-day execution to the top objectives of the company, and they are responsible for making sure the top objectives are well communicated to team members.
1. They work peer-to-peer and sponsor healthy conflict amongst the team to resolve issues quickly, escalating only when all options are exhausted.
1. They ambitiously define roles for, grow and hire their teams for what is needed from the business in the next 3-4 years.
1. They coach their teams to work within the [communication guidelines](/handbook/communication/) and lead by example.

### Senior leaders

The title of a senior leader can be either VP or Senior Director.
They are all members of our [S-group](/company/team/structure/#s-group).

#### S-group

In some divisions, senior leaders map to departments in the [cost center](/handbook/finance/financial-planning-and-analysis/#cost--reporting-structure) structure.

Members of the S-group are expected to demonstrate leadership in the way all members of the [director group](/company/team/structure/#director-group) are, plus:

1. They have significant strategic and functional responsibility.
1. They have significant operating budget responsibility (things like compensation planning, budget allocation, negotiation, investment tradeoff decisions).
1. They have leverage and influence over the performance and success of large teams (both reporting directly and indirectly) and their success will result in increased success across large numbers of people.
1. The impact of their decisions are broad and significant.
1. Some of them have critical external responsibility to represent the company and make decisions and statements for which the company is accountable.

### Executives

The executive layer is structured as follows. There are two primary processes, product (product management, engineering) and go-to market (marketing and sales).
Some companies have a Chief Product Officer (CPO) for the former and a Chief Operating Officer (COO) for the latter.
We have a flatter organization.
The C-level exec for product is the CEO and the EVPs of Product Management, Product Strategy, and Engineering report to the CEO. Marketing and sales have separate executives.
The three enabling functions, legal, finance and people, also each have a C-level executive, the Chief Legal Officer (CLO), Chief Financial Officer (CFO) and Chief People Officer (CPO).
Together, these executives consist of the [E-group](/company/team/structure/#e-group)
They meet weekly, attend quarterly board meetings, have a [public Slack channel #e-group](https://gitlab.slack.com/messages/C5W3VS1C4) for most discussion topics, as well as a private one for rare confidential matters.

Except for Sales and Marketing, there are usually multiple executives to a [cost center](/handbook/finance/financial-planning-and-analysis/#cost--reporting-structure). For example,CLO, CFO, CEO, and CPO all fall under the G&A (General & Administrative Expenses) Cost Center.

#### E-group

Members of the E-group are expected to demonstrate leadership in the way all members of the [S-group](/company/team/structure/#s-group) are, plus:
1. They suggest relevant, ambitious, and quantifiable OKRs and achieve 70% of them.
1. They are reliable and ensure their teams complete what they agreed to do.
1. They are proactive about detecting and communicating problems in their functions before other departments even notice them.
1. They hire and retain leaders that perform better in their functional areas.
1. They create roles and set requirements for what is needed 3-4 years out and hire for that profile.
1. They get a huge amount of things done by iterating quickly and training their department in iteration.
1. They define and communicate the business strategy and vision, instead of being overly tactical in the business.
1. They share insights about other functional areas that make others better at their job.
1. They suggest and implement improvements to our cross-functional processes.
1. They frequently help achieve results outside their function.
1. They make other executives better in their discipline.

### Board Members

[Board Members](/job-families/board-of-directors/board_member/) serve on the GitLab board and participate in board meetings and board committees, as well as other responsibilities.

## Organizational Structure

* **Divisions:** the area under one executive. _e.g._ the Engineering division.
* **Departments:** lead by Directors or VPs and comprise multiple teams or sub-departments _e.g._ the Infrastructure department within the Engineering division
* **Sub-departments:** lead by Directors or Senior Managers and comprised of multiple teams _e.g._ the Enablement Sub-department within the Development department
* **Teams:** constitute departments  and are made of a line manager and their direct reports _e.g._ the Security operations team within the Security Department

**Note** - within the Engineering and Product divisions we try to maintain a close relationship between our organizational structure and our [Product Hierarchy](/handbook/product/product-categories/#hierarchy) in order to maintain stable counterparts in our organizational structure.

Finance also has a notion called "departments" for financial planning purposes. But these do not align with our organizational departments. For instance the finance department "product development" rolls up both the PM and Engineering functions. But it excludes the Support department, which is part of the engineering function, but a different budget. This name collision should probably be resolved in the future. For futher reference see our [department roll up structure](/handbook/finance/financial-planning-and-analysis/#cost--reporting-structure) for accounting purposes.

We try our best to keep our handbook and documentation up to date, but certain terms used in the past have been updated through time. The term **Functional Group** is no longer used, and it currently is encompassed by the term **Departments**. The term **Functional Group leader** is no longer used, and it is currently encompassed by the term [E-group](/company/team/structure/#e-group) leaders.

### Organized by Output

In many ways, we are organized by output.
This way  we can ensure that responsibilities don't overlap.
We also ensure every department has a clear priority.

| Division | Output |
|-------------|--------------------------------|
| Marketing | Generate Pipeline |
| Sales | Close Pipeline |
| Product | Prioritize development |
| Engineering | Execute development |
| People | Enable people |
| Finance | Ensure correctness |
| Legal | Ensure compliance  |

### Product Groups
{: #product-groups}

Our engineering organization is directly aligned to groups as defined in [product category hierarchy](/handbook/product/product-categories/#hierarchy).
Our groups operate on the principle of [stable counterparts](/handbook/leadership/#stable-counterparts) from multiple functions.

For example, we have a Product Manager, Product Marketing Manager, Engineering Manager, Content Marketer, Backend Developers, Frontend Developers, and Product Designers that are all dedicated to a group called "Package". Collectively, these individuals form the "Package group". The word "Package" appears in their titles as a specialty, and in some cases, their team name.

A group has no reporting lines because we [don't want a matrix organization](/handbook/leadership/#no-matrix-organization).
Instead, we rely on stable counterparts to make a group function well.
In some shared functions, like design, technical writing and quality individuals are paired to one or more stages so that there are stable counterparts.

While everyone can contribute, the scope of a group should be non-overlapping and not a required dependency for other groups to deliver value to users.
This facilitates [results](/handbook/values/#results), [iteration](/handbook/values/#iteration) and [efficiency](/handbook/values/#efficiency).

Internal platform groups (those focused on a non-user facing part of our product, like a set of internal APIs) tend to [create heavy coordination costs](https://anthonysciamanna.com/2017/03/05/remove-cross-team-dependencies.html) on other groups which depend on platform improvements to deliver valuable features to users. In order to stay efficient, it is important to ensure each group is non-blocking and is able to deliver value to users directly. This is why we avoid internal platform groups.

It is also important to ensure a group doesn't have a scope definition that is shared across multiple groups. Here are two examples:

1. We don't have an internationalization group.
   That responsibility is shared across many groups.
   We might instead have an internationalization tooling group.
1. We don't have a performance group.
   Ensuring GitLab is performant is the responsibility of all groups.
We do have the [Memory group](/handbook/product/product-categories/#memory-group) and [Database group](/handbook/product/product-categories/#database-group) whose charters involve consulting and providing guidance for other teams, but should be considered non-blocking.

The [Memory group](/handbook/product/product-categories/#memory-group) focused on the specifics of reducing GitLab's memory footprint, creating documentation and tooling to assist in memory related concerns. 

The [Database group](/handbook/product/product-categories/#database-group) is focused on the specifics of database management/scaling and to provide consulting for development teams in need of database development guidance.  While database related merge requests still require approval from a database maintainer our [database review](https://docs.gitlab.com/ee/development/database_review.html) process has necessarily scaled beyond just the members of the database team.

### Working Groups

A [working group](/company/team/structure/working-groups/) is a specific type of group that assembles for a period of time to accomplish a business goal. Working groups have defined responsibilities and meet regularly. Ideally a working group can disband when the goal is complete to avoid accrued bureaucracy.

### Middle Management

Middle managers are team members who do not report to the CEO and have managers of people reporting to them.
It is not defined by someone's title or their place in the org chart, but by these two criteria.

## Roles

People can be a specialist in one thing and be an expert in multiple things. These are listed on the [team page](/company/team/).

### Specialist

Specialists carry responsibility for a certain topic.
They keep track of issues in this topic and/or spend the majority of their time there.
Sometimes there is a lead in this topic that they report to.
You can be a specialist in only one topic.
The specialist description is a paragraph in the job description for a certain title.
A specialist is listed after a title, for example: Developer, database specialist (do not shorten it to Developer, database).
Many specialties represent stable counterparts. For instance, a "Software Engineer in Test, Create" specializes in the "Create" [stage group](#stage-groups) and is dedicated to that group.
If you can have multiple ones and/or if you don't spend the majority of your time there it is probably an [expertise](/company/team/structure/#expert).
Since a specialist has the same job description as others with the title they have the same career path and compensation.

### Expert

Expert means you have above average experience with a certain topic.
Commonly, you're expert in multiple topics after working at GitLab for some time.
This helps people in the company to quickly find someone who knows more.
Please add these labels to yourself and assign the merge request to your manager.
An expertise is not listed in a role description, unlike a [specialist](/job-families/specialist).

For Production Engineers, a listing as "Expert" can also mean that the individual
is actively [embedded with](/handbook/engineering/infrastructure/#embedded) another team.
Following the period of being embedded, they are experts in the regular sense
of the word described above.

Developers focused on Reliability and Production Readiness are named [Reliability Expert](/job-families/expert/reliability/).

### Mentor

Whereas an expert might assist you with an individual issue or problem, mentorship is about helping someone grow their career, functional skills, and/or soft skills. It's an investment in someone else's growth.

Some people think of expertise as hard skills (Ruby, International Employment Law, etc) rather than soft skills (managing through conflict, navigating career development in a sales organization, etc).

If you would like to be a mentor in a certain area, please add the information to the team page. It is important to note whether you would like to be a mentor internally and/or externally at GitLab. Examples of how to specify in the expertise section of the team page: `Mentor - Marketing, Internal to GitLab` or `Mentor - Development (Ruby), External and Internal to GitLab`.

### GitLab.com isn't a role

Some of the things we do make are GitLab.com specific, but we will not have GitLab.com specific people, meetings, or [product KPIs](/handbook/business-ops/data-team/metrics/#product-kpis).
We want to optimize for IACV and customer success and .com is simply a way to deliver that.
Our innovation and impact will slow down if we need to maintain two separate products and focus our energy on only one of them.
The majority of work in any role applies to both ways of delivery GitLab, self-managed and .com.

1. We have a functionally organized company, the functions need to as mutally exclusive as possible to be efficient, .com overlaps with a small part of many functions.
1. Having .com specific people will increase the pressure to get to two codebases, that can be a big hindrance: "splitting development between two codebases and having one for cloud and one for on-prem is what doomed them", and "they split cloud and on-prem early on and it was a 10-year headache with the OP folks feeling left in line to jump in the pool but never could.  While cloud pushed daily/weekly with ease, OP was _easily_ 6-mo behind leaving customers frustrated"
1. The [reasons .com customers churned](https://drive.google.com/file/d/1QhGrofKbiUIJSv7ZI524FshoKnS-6y-P/edit) were all things that occur in both self-managed and .com
1. Improvements we can make in user growth might be informed by .com specific data but can be implemented for both delivery mechanisms.

#### Exception: Product Management Senior Leader

We do have an exception to the above, which is a senior leader in Product Management that is responsible for the cross-functional outcomes needed on GitLab.com. This is because GitLab.com is a large operational expense, it's also potentially a large source of IACV, and because it's strategically important that we have a thriving SaaS offering as more of the world gets comfortable hosting their source code in the cloud.

Here are some examples of the things that this senior leader will coordinate:

* Growth Group: [Stages per User (SpU)](/handbook/product/metrics/#stages-per-user-spu)
* Pricing: [Tiers](/pricing/)
* Infrastructure Department: Cloud spend (within limits, not cost per user)
* Development Department: Prioritization of large enterprise features

## Other Considerations

### The word "Manager" in a title doesn't imply people management or structure

Some of individual contributors (without any direct reports) have manager in their title without a comma after it. These titles are not considered a people manager in our company structure nor salary calculator, examples are product manager, accounting manager, account manager, channel sales manager, technical account manager, field marketing managers, online marketing manager, and product marketing manager. People with manager and then a comma are a people manager in our company structure.

### Wider community

GitLab is a project bigger than GitLab the company.
It is really important that we see the community around GitLab as something that includes the people at the company.
When you refer to the community excluding the people working for the company please use: wider community.
If referring to both people at the company and outside of it use community or GitLab team-members.

### Team and team-members

Team is reserved for the smallest group.
It is defined by a manager and their reports.
It does not refer to a [group](#groups) or [a department](/handbook/engineering/development/).

We  refer to all the people working for the company as team-members.
This is a bit confusing since team is reserved for the smallest group but it is preferable over all the alternatives we considered:

1. Employees since we have many contractors working for GitLab Inc.
1. GitLab team members since this should include the [wider community](/handbook/communication/#writing-style-guidelines).
1. Incers (referring to GitLab Inc.) since it sounds dull.
1. Staff is what is on [our user profile if we work for GitLab Inc.](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/29480/diffs) but is also an [engineering level](/job-families/engineering/backend-engineer/#staff-backend-engineer).
1. Tanuki (referring to our logo) since [it is confusing to refer to humans with an animal species](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24447/).
