---
layout: markdown_page
title: "FY21-Q3 OKRs"
---

This [fiscal quarter](/handbook/finance/#fiscal-year) will run from August 1, 2020 to October 31, 2020.

## On this page
{:.no_toc}

- TOC
{:toc}

## OKR Schedule
The by-the-book schedule for the OKR timeline would be

| OKR schedule | Week of | Task |
| ------ | ------ | ------ |
| -5 | 2020-06-30 | CEO shares top goals with E-group for feedback |
| -4 | 2020-07-06 | CEO pushes top goals to this page |
| -3 | 2020-07-13 | E-group pushes updates to this page |
| -2 | 2020-07-20 | E-group 50 minute draft review meeting |
| -2 | 2020-07-22 | E-group discusses with teams |
| -1 | 2020-07-27 | CEO reports give a How to Achieve presentation |
| 0  | 2020-08-03 | CoS updates OKR page for current quarter to be active |
| +2 | 2020-08-17 | Review previous and next quarter during the next Board meeting |

## OKRs

### 1. CEO: IACV
[Epic 720](https://gitlab.com/groups/gitlab-com/-/epics/720)
1. **CEO KR:** Implement 5 initiatives in sales to increase [IACV](/handbook/sales/#incremental-annual-contract-value-iacv) by $XM.
2. **CEO KR:** Achieve a [CAC Revenue Payback Ratio](https://about.gitlab.com/handbook/sales/#cac-to-revenue-payback-ratio) < $X.
    1. **CRO**: Deliver three pilot programs to increase iACV / decrease Sales and Marketing expense. [Epic 797](https://gitlab.com/groups/gitlab-com/-/epics/797)
    1. **CFO**: Drive strategic alignment through financial insights
3. **CEO KR:** Double down on what works in marketing. Only do marketing activities that >X CWlinear to spend. 
4. EVP of Engineering: [Run infrastructure more efficiently](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8303)

### 2. CEO: Popular next generation product
[Epic 722](https://gitlab.com/groups/gitlab-com/-/epics/722)
1. **CEO KR:** Continue winning against Microsoft Azure GitHub. Improve our competitive win rate from X to Y. 
    1. **EVP, Product:** Web and Git response time performance for GitLab.com as fast as GitHub.com [KR's in Epic 732](https://gitlab.com/groups/gitlab-com/-/epics/732)
2. **CEO KR:** Drive total monthly active users, [TMAU](https://about.gitlab.com/handbook/product/performance-indicators/#total-monthly-active-users-tmau) and [Paid TMAU](https://about.gitlab.com/handbook/product/performance-indicators/#paid-total-monthly-active-users-paid-tmau), from X to Y.
    1. **EVP, Product:** Increase TMAU by 17% and Paid TMAU from X to Y to help drive paid conversion [KR's in Epic 731](https://gitlab.com/groups/gitlab-com/-/epics/731)
    1. **VP, Product:** Shift R&D organization to become Pajamas First to improve efficiency and user experience [KR's in Epic 734](https://gitlab.com/groups/gitlab-com/-/epics/734)
3. **CEO KR:** Complete three key packaging projects. 
    1. **EVP, Product**:  Complete mission critical packaging projects to help improve product margins [KR's in Epic 733](https://gitlab.com/groups/gitlab-com/-/epics/733)
4. EVP of Engineering: [Increase dogfooding, performance, and productivity](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8303)

### 3. CEO: Great team
[Epic 721](https://gitlab.com/groups/gitlab-com/-/epics/721)
1. **CEO KR:** Exceed 50% top of funnel from outreach and 90% of outreach to diverse candidates.
    1. **CFO**: Institute public company financial reporting standards
2. **CEO KR:** Certify 25 non-engineering team members in self-serve data via handbook first materials
3. **CEO KR:** Certify 25% of people leaders through manager enablement certification.
    1. **CFO**: Deliver handbook first training and Self-Serve Data capabilities for product geolocation analysis and paying customer firmographic analysis.
4. EVP of Engineering: [Expand the capabilities of our team](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8303)

## How to Achieve Presentations

* Engineering
* Finance
* Legal
* Marketing
* People
* [Product](https://youtu.be/peoaBL4hbiI)
* Sales
