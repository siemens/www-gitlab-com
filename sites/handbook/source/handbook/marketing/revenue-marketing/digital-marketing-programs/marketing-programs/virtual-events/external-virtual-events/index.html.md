---
layout: markdown_page
title: "External Virtual Events"
---

# Types of external virtual events

External virtual events are, by definition, not owned and hosted by GitLab. They are hosted by an external third party (i.e. DevOps.com). The two types of external virtual events are below, and involve epic and issue creation, designation of DRIs, and workback schedule definition between requester (commonly FMM or Corporate events) and Marketing Programs.

* [Sponsored Webcasts](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/external-virtual-events/#sponsored-webcast): A sponsored webcast is hosted on an external partner/vendor platform (e.g: DevOps.com). The partner/vendor is responsible for driving registration, moderating and hosting the webcast on their platform, and delivering a lead list after the event.
* [Virtual Conferences](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/external-virtual-events/#sponsored-virtual-conference): In a virtual conference, GitLab will pay a sponsorship fee to receive a virtual booth and sometimes a speaking session slot or panel presence. **Presence of a virtual booth is a requirement for the external virtual event to be considered a Virtual Conference.**

## 📌 Sponsored Webcast

*A sponsored webcast is hosted on an external partner/vendor platform (e.g: DevOps.com). The partner/vendor is responsible for driving registration, moderating and hosting the webcast on their platform, and delivering a lead list after the event. The project owner (corporate events or field marketing in most cases) is responsibble for creating the epic and related issue creation, and keeping timelines and DRIs up-to-date. Mareting Ops is responsible for uploading the list to our database and Marketing Programs is responsible for sending post-event follow-up emails (when relevant)*

### Process in GitLab to organize epic & issues

The project owner is responsible for following the steps below to create the epic and related issues in GitLab.

1. Project owner (FMM) creates the main tactic issue
1. Project owner (FMC) creates the epic to house all related issues (code below)
1. Project owner (FMC) creates the relevant issues required (shortcut links in epic code below)
1. Project owner (FMC) associates all the relevant issues to the newly created epic, as well as the original issue
1. Project owner (FMC) sets due dates for each issue, based on agreed upon SLAs between teams for each task, and confirms accurate ownership for each issue

*Note: if the date of the tactic changes, FM/requester is responsible for changing the due dates of all related issues to match the new date, and alerting the team members involved.*

### Code for epic

```
<!-- Name this epic: Sponsored Webcast - [Vendor] - [3-letter Month] [Date], [Year] -->

## [Main Issue >>]()

## [Copy document >>]() - [template](https://docs.google.com/document/d/1j43mf7Lsq2AXoNwiygGAr_laiFzmokNCfMHi7KNLjuA/edit)

## :notepad_spiral: Key Details 
* **Project Owner/Field Marketing Manager:** 
* **Marketing Programs:**  
* **Field Marketing Coordinator:** 
* **Type:** Sponsored Webcast
* **Official Name:** 
* **Registration URL:** 
* **Account Centric?** Yes/No
* **Budget:** 
* **Campaign Tag:**  
* **Launch Date:**  [YYYY-MM-DD] 
* [ ] [main salesforce program]()
* [ ] [main marketo campaign]()
* [ ] Campaign UTM - Project Owner/FM to fill in (Format: campaign tag - change to all lowercase, no spaces, hyphens, underscores, or special characters)

## User Experience
[Project Owner/FMM to provide a description of the user journey - from communication plan, to what the user experiences upon receipt, plus triggers on our end like confirmation email and how GitLab fulfils with the vendor, up until receipt by the user and answering whether or not we get confirmation that they received it... what is the anticipated journey after that?]

## Additional description and notes about the tactic
[Project Owner/FMM add whatever additional notes are relevant here]

## Issue creation

* [ ] [Facilitate tracking issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-01-facilitate-tracking) - FMC creates, assigns to FMC or MPM (depending on [program type](https://about.gitlab.com/handbook/marketing/marketing-operations/#marketo-program-and-salesforce-campaign-set-up))
* [ ] [List clean and upload issue created](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=event-clean-upload-list) - FMC creates, assigns to FMM and MOps
* [ ] [Follow up email issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-04-follow-up-email) (*optional*) - FMC creates, assigns to FMM, FMC and MPM
* [ ] [Add to nurture issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-05-add-to-nurture) - FMC creates, assigns to FMM and MPM
* [ ] Optional*: [Marketo landing page copy issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=marketo-landing-page-copy) - Corp/FMC creates, assign to Corp/FMM and FMC
* [ ] Optional*: [Marketo landing page creation issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=marketo-landing-page-creation) - Corp/FMC creates, assign to MPM

**Optional: create the optional issues only if we have rights to recording and content is worth gating*

/label ~"Marketing Programs" ~"mktg-status::wip" ~"Webcast - Sponsored" ~"Field Marketing"
```

☝️ *Note on campaign utm format: we avoid using special characters due to issues in the past passing UTMs from Bizible to SFDC, the basis for attribution reporting.*

## 📌 Sponsored Virtual Conference

*In a virtual conference, GitLab will pay a sponsorship fee to receive a virtual booth and sometimes a speaking session slot or panel presence. The owner (FM in most cases) is responsible for the epic and related issue creation. Mktg-OPs will be responsible for uploading the list to our database and MPMs will be responsible for sending post-event follow-up emails (when relevant).*

**Presence of a virtual booth is a requirement for the virtual event to be considered a Virtual Conference.** [Link to Marketo program template that will be cloned.](https://app-ab13.marketo.com/#ME4739A1)

### Process in GitLab to organize epic & issues

The project owner is responsible for following the steps below to create the epic and related issues in GitLab.

1. Project owner (Corp/FMM) creates the main tactic issue
1. Project owner (Corp/FMC) creates the epic to house all related issues (code below)
1. Project owner (Corp/FMC) creates the relevant issues required (shortcut links in epic code below)
1. Project owner (Corp/FMC) associates all the relevant issues to the newly created epic, as well as the original issue
1. Project owner (Corp/FMC) sets due dates for each issue, based on agreed upon SLAs between teams for each task, and confirms accurate ownership for each issue

*Note: if the date of the tactic changes, Corp, FM, or other requester is responsible for changing the due dates of all related issues to match the new date, and alerting the team members involved.*

### Code for epic

```
<!-- Name this epic: Sponsored Virtual Conference - [Vendor] - [3-letter Month] [Date], [Year] -->

## [Main Issue >>]()

## [Copy document >>]() - [template](https://docs.google.com/document/d/1j43mf7Lsq2AXoNwiygGAr_laiFzmokNCfMHi7KNLjuA/edit)

## :notepad_spiral: Key Details 
* **Corp Events/Field Marketing Manager/Requester:** 
* **Marketing Programs:**  
* **Field Marketing Coordinator:** 
* **Type:** Sponsored Virtual Conference
* **Official Name:** 
* **Registration URL:** 
* **Account Centric?** Yes/No
* **Budget:** 
* **Campaign Tag:**  
* **Launch Date:**  [YYYY-MM-DD] 
* [ ] [main salesforce program]()
* [ ] [main marketo campaign]()
* [ ] Campaign UTM - Project Owner to fill in (Format: campaign tag - change to all lowercase, no spaces, hyphens, underscores, or special characters)

## User Experience
[Project owner to provide a description of the user journey - from communication plan, to what the user experiences upon receipt, plus triggers on our end like confirmation email and how GitLab fulfils with the vendor, up until receipt by the user and answering whether or not we get confirmation that they received it... what is the anticipated journey after that?]

## Additional description and notes about the tactic
[Project owner add whatever additional notes are relevant here]

## Issue creation

* [ ] Prep: [Facilitate tracking issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-01-facilitate-tracking) - Corp/FMC creates, assigns to Corp/FMC or MPM (depending on [program type](https://about.gitlab.com/handbook/marketing/marketing-operations/#marketo-program-and-salesforce-campaign-set-up))
* [ ] Post: [List clean and upload issue created](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=event-clean-upload-list) - Corp/FMC creates, assigns to Corp/FMM and MOps
* [ ] Post: [Follow up email issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-04-follow-up-email) (*optional*) - Corp/FMC creates, assigns to Corp/FMM, FMC and MPM
* [ ] Post: [Add to nurture issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-05-add-to-nurture) - Corp/FMC creates, assigns to Corp/FMM and MPM
* [ ] Post*: [Marketo landing page copy issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=marketo-landing-page-copy) - Corp/FMC creates, assign to Corp/FMM and FMC
* [ ] Post*: [Marketo landing page creation issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=marketo-landing-page-creation) - Corp/FMC creates, assign to MPM

<details>
<summary>Corporate Marketing Activation: Expand below for quick links to issues to be created and linked to the epic.</summary>

* [ ] Activate*: [Organic Social Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/issues/new?issuable_template=social-general-request) - Corp creates, assignment in issue
* [ ] Activate*: [Blog Issue](https://gitlab.com/gitlab-com/marketing/growth-marketing/global-content/content-marketing/issues/new#?issuable_template=blog-post-pitch) - Corp creates, assignment in issue
* [ ] Activate*: [PR Announcement Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/issues/new?issuable_template=announcement) - Corp creates, assignment in issue

</details>

**Everything with an * is optional: create the optional issues only if we plan to use those outbound activation channels, and/or have rights to the recording(s) of the content and is worth gating (with a further plan for post-live content activation).*

/label ~"Marketing Programs" ~"mktg-status::wip" ~"Virtual Conference" ~"Corporate Marketing"
```

## Adding sponsored virtual events into the calendar

The [sponsored virtual events](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV8xOGVqOHN0NmlxajZpYXB1NTNrajUzNHBsa0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t) calendar will be used to log all planned and scheduled sponsored webcasts and virtual conferences. **The purpose of this calendar is to provide visibility and help the sponsoring team minimize topic overlap with other GitLab virtual events happening around the same time and to provide executive visibility into all GitLab sponsored virtual events.**

**DRI adding to sponsored virtual events calendar: Sponsor owner**

##### Planned sponsored virtual events

As soon as you create the epic for the sponsored virtual event, add the event to the sponsored virtual events calendar by creating an event on the day the sponsored virtual event will be live.Make sure to also include the link to the epic in the calendar description.
* For sponsored webcast please use the following naming convention `[Hold WC sponsored] Event title` (e.g: `[Hold WC sponsored] Securing your pipeline with GitLab and WhiteSource`).
* For sponsored virtual conferences, please use the following naming convention  `[Hold VC sponsored] Event title` (e.g: `[Hold VC sponsored] Predict 2021`).

##### Confirmed sponsored virtual events 

Once the sponsorship has been confirmed, go to your calendar event and remove `Hold` from the event title. **Note:** In the spirit of efficiency and to avoid creating multiple calendar invites, please include the epic or issue, add the marketing DRI, any GitLab speakers and/or attendees (SALs, SAs, etc.), as well as any other team members who would benefit from being included in the calendar invite as this invite will provide a hold for team members participating in the event. The 3rd party sponsor will send out additional event details separately.
* For sponsored webcasts, change the event title to `[WC sponsored] Event title` (e.g: `[WC sponsored] Securing your pipeline with GitLab and WhiteSource`).
* For sponsored virtual conferences, change the event title to  `[VC sponsored] Event title` (e.g: `[VC sponsored] Predict 2021`).

##### Rescheduled sponsored virtual events

If the sponsorship needs to be rescheduled, please update the date/time of the event on the sponsored virtual events calendar.

##### Canceled sponsored virtual events

If the sponsorship is canceled, please remove the event from the sponsored virtual events calendar.


