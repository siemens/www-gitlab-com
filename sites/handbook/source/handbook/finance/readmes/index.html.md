---
layout: handbook-page-toc
title: "GitLab Finance Team READMEs"
---

## Finance Team READMEs

- [Chase Wrights's README (Manager, Corporate FP&A)](/handbook/finance/readmes/chasewright/)
- [Melody Maradiaga's README](Sr. Manager, Accounting and External Reporting)] (/handbook/finance/readmes/mmaradiaga)

