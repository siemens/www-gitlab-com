---
layout: handbook-page-toc
title: "Data Team Organization"
description: "GitLab Data Team Organization"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

----

## <i class="far fa-compass fa-fw icon-color font-awesome" aria-hidden="true"></i> Team Organization 

### Data Analysts

The Data Analyst Team operates in a hub and spoke model, where some analysts are part of the central data team (hub) while others are embedded (spoke) or distributed (spoke) throughout the organization.

**Central** - those in this role report to and have their priorities set by the Data team. They currently support those in the Distributed role, cover ad-hoc requests, and support all Departments.

**Embedded** - those in this role report to the data team but their priorities are set by their Departments.

**Distributed** - those in this role report to and have their priorities set by their Departments. However, they work closely with those in the Central role to align on data initiatives and for assistance on the technology stack.

All roles mentioned above have their MRs and dashboards reviews by members in the Data team. Both Embedded and Distributed data analyst or data engineer tend to be subject matter experts (SME) for a particular business unit.

#### Data Support

| Role             | Team Member                                    | Primary Business Division       | Primary Business Partner                 | Board | 
| -------------    | ---------------------------------------------- | ------------------------------- | ---------------------------------------- | ----- | 
| Manager, Data    | [@kathleentam](https://gitlab.com/kathleentam) | Data Team                       | [@rparker2](https://gitlab.com/rparker2) | [Board](https://gitlab.com/groups/gitlab-data/-/boards/1446961?assignee_username=kathleentam&) | 
| Jr. Data Analyst | [@ken_aguilar](https://gitlab.com/ken_aguilar) | Engineering                     | [@edjdev](https://gitlab.com/edjdev)     | [Engineering Board](https://gitlab.com/groups/gitlab-data/-/boards/1786931?&label_name[]=Engineering)      | 
| Data Analyst     | [@iweeks](https://gitlab.com/iweeks)           | Finance                         | [@bryanwise](https://gitlab.com/bryanwise), [@cmestel](https://gitlab.com/cmestel), [@cmestel](https://gitlab.com/cmestel) | [Finance Board](https://gitlab.com/groups/gitlab-data/-/boards/1312981?assignee_username=iweeks&&label_name[]=Finance) | 
| Jr. Data Analyst | [@jeanpeguero](https://gitlab.com/jeanpeguero) | Marketing                       | TBD | [Marketing Board](https://gitlab.com/groups/gitlab-data/-/boards/1486061?&label_name[]=Marketing) | 
| Data Analyst     | [@derekatwood](https://gitlab.com/derekatwood) | Sales                           | TBD | [Sales Board](https://gitlab.com/groups/gitlab-data/-/boards/1529937?&label_name[]=Sales) | 
| Sr. Data Analyst | [@Pluthra](https://gitlab.com/Pluthra)         | People                          | TBD | [People Board](https://gitlab.com/groups/gitlab-data/-/boards/1435002?label_name[]=People) | 
| Sr. Data Analyst | [@mpeychet](https://gitlab.com/mpeychet)       | Product                         | TBD | [Product Board](https://gitlab.com/groups/gitlab-data/-/boards/1509536?&label_name[]=Product) | 


### Data Engineers

[Data Engineer Handbook](/handbook/business-ops/data-team/organization/engineering)

Though Data Engineers are sometimes given assignments to better support business functions **no members of the data engineering team are embedded or distributed**. This allows them to focus on our data platform with an appropriate development cadence.

| Role | Team Member | Assigned Groups | Timezone |  Prioritization Owners | 
| ------ | ------ | ------ | ------ | ------ |
| Manager, Data | [@jjstark](https://gitlab.com/jjstark) | Data Engineering | US Pacific (UTC-7) | [@rparker2](https://gitlab.com/rparker2) | 
| Staff Data Engineer | [@tayloramurphy](https://gitlab.com/tayloramurphy) | People | US Central (UTC-5) | [@jjstark](https://gitlab.com/jjstark) |
| Senior Data Engineer | [@m_walker](https://gitlab.com/m_walker) | Product & Engineering | US Central (UTC-5) | [@jjstark](https://gitlab.com/jjstark) |
| Data Engineer | [@msendal](https://gitlab.com/msendal) | Finance | Central Europe (UTC+2) | [@jjstark](https://gitlab.com/jjstark) |
| Data Engineer | [@paul_armstrong](https://gitlab.com/paul_armstrong) | Sales & Marketing | Central Africa (UTC+2) |[@jjstark](https://gitlab.com/jjstark) |

### Fusion Teams

The above assignments effectively create the following teams that work together to support the various divisions at GitLab

| Division | Data Analyst | Data Engineer |
|---|---|---|
| Engineering | [@ken_aguilar](https://gitlab.com/ken_aguilar) | [@m_walker](https://gitlab.com/m_walker) |
| Product | [@mpeychet](https://gitlab.com/mpeychet) | [@m_walker](https://gitlab.com/m_walker) |
| Finance | [@iweeks](https://gitlab.com/iweeks) | [@msendal](https://gitlab.com/msendal) |
| Marketing| [@jeanpeguero](https://gitlab.com/jeanpeguero) | [@paul_armstrong](https://gitlab.com/paul_armstrong) |
| Sales | [@derekatwood](https://gitlab.com/derekatwood) | [@paul_armstrong](https://gitlab.com/paul_armstrong) |
| People | [@Pluthra](https://gitlab.com/Pluthra) | [@tayloramurphy](https://gitlab.com/tayloramurphy) |

## Job Descriptions
#### Data Analyst
[Job Family](/job-families/finance/data-analyst/){:.btn .btn-purple}

#### Data Engineer
[Job Family](/job-families/finance/data-engineer/){:.btn .btn-purple}

#### Manager
[Job Family](/job-families/finance/manager-data){:.btn .btn-purple}

#### Director of Data and Analytics
[Job Family](/job-families/finance/dir-data-and-analytics){:.btn .btn-purple}
