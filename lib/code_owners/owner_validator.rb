require 'yaml'

module Gitlab
  module CodeOwners
    class OwnerValidator
      TEAM_YML = ::File.expand_path('../../data/team.yml', __dir__).freeze
      ALLOWED_GROUPS = %w[@timtams @gl-product-leadership].freeze

      def initialize(data)
        @data = data
        @team_entries = YAML.load_file(TEAM_YML)
        @errors = []
      end

      attr_reader :errors

      def valid?
        data.values.flatten.uniq.each do |owner|
          errors << missing_owner_message(owner) unless owner_exists?(owner)
        end

        errors.none?
      end

      private

      attr_reader :data, :team_entries

      def owner_exists?(owner)
        return true if owner.start_with?('@gitlab-')
        return true if ALLOWED_GROUPS.include?(owner)

        team_entries.find { |entry| entry['gitlab']&.downcase == owner.gsub(/@/, '')&.downcase }
      end

      def missing_owner_message(owner)
        "Incorrect owner: #{owner}. Make sure that this Gitlab account name is correct. It must be present in data/team.yml."
      end
    end
  end
end
