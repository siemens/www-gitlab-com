require 'generators/direction'
require 'generators/releases'
require 'generators/org_chart'
require 'extensions/breadcrumbs'
require 'extensions/only_debugged_resources'
require 'extensions/partial_build'
require 'extensions/listen_monkeypatch'
require 'extensions/codeowners'
require 'lib/homepage'
require 'lib/mermaid'
require 'lib/plantuml'

#----------------------------------------------------------
# Global config (not specific to development or build mode)
#----------------------------------------------------------

# Settings
set :haml, { format: :xhtml }
set :markdown_engine, :kramdown
set :markdown, tables: true, hard_wrap: false, input: 'GFM'

# Disable HAML warnings
# https://github.com/middleman/middleman/issues/2087#issuecomment-307502952
Haml::TempleEngine.disable_option_validator!

# Paths with custom per-page overrides
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false
page '/404.html', directory_index: false

# Don't render or include the following into the sitemap
ignore '/direction/*' unless ENV['PRIVATE_TOKEN']
ignore '/frontend/*'
ignore '/templates/*'
ignore '/includes/*'
ignore '/upcoming-releases/template.html'
ignore '/releases/template.html'
ignore '/releases/gitlab-com/template.html'
ignore '/company/team/structure/org-chart/template.html'
ignore '/source/stylesheets/highlight.css'
ignore 'source/job-families/check_job_families.py'
ignore '/category.html'
ignore '/.gitattributes'
ignore '**/.gitkeep'
ignore '/sites/*'
ignore '/jobs/apply/interim_landing_page.html.haml'
ignore '/jobs/apply/job-listing/template.html'

# Extensions
activate :syntax, line_numbers: false
activate :autoprefixer do |config|
  config.browsers = ['last 2 versions', 'Explorer >= 9']
end
activate :breadcrumbs, wrapper: :li, separator: '', hide_home: true, convert_last: false
activate :codeowners
if ENV['MIDDLEMAN_DEBUG_RESOURCE_REGEX']
  ::Middleman::Extensions.register(:only_debugged_resources, OnlyDebuggedResources)
  activate :only_debugged_resources
end
activate :partial_build # Handle splitting of files across CI jobs for paths handled by top-level build

# Blog and Release extensions
unless ENV['SKIP_BLOG']
  activate :blog do |blog|
    blog.name = 'blog'
    # This will add a prefix to all links, template references and source paths
    blog.prefix = 'blog'
    blog.sources = 'blog-posts/{year}-{month}-{day}-{title}.html'
    blog.permalink = '/{year}/{month}/{day}/{title}/index.html'
    blog.layout = 'post'
    # Allow draft posts to appear on all branches except master (for Review Apps)
    blog.publish_future_dated = true if ENV['CI_COMMIT_REF_NAME'].to_s != 'master'

    blog.summary_separator = /<!--\s*more\s*-->/

    blog.custom_collections = {
      categories: {
        link: '/categories/{categories}/index.html',
        template: '/category.html'
      }
    }
  end

  activate :blog do |blog|
    blog.name = 'releases'
    # This will add a prefix to all links, template references and source paths
    blog.prefix = 'releases'
    blog.sources = 'posts/{year}-{month}-{day}-{title}.html'
    blog.permalink = '/{year}/{month}/{day}/{title}/index.html'
    blog.layout = 'post'
    # Allow draft posts to appear on all branches except master (for Review Apps)
    blog.publish_future_dated = true if ENV['CI_BUILD_REF_NAME'].to_s != 'master'

    blog.summary_separator = /<!--\s*more\s*-->/

    blog.custom_collections = {
      categories: {
        link: '/categories/{categories}/index.html',
        template: '/category.html'
      }
    }
  end
end

# External Pipeline
if !ENV['SKIP_EXTERNAL_PIPELINE'] && !ENV['CI_SERVER']
  # NOTE: This only applies to 'development' mode and local non-CI builds. On CI, the asset build and deploy is handled by a dedicated job, not this Middleman external_pipeline
  # TODO: Asset builds should be per-site. Instead of the all asset bundles being built at once, it can be delegated to individual sites to build their own bundles.
  activate :external_pipeline,
           name: :frontend,
           command: "source/frontend/pipeline.sh #{build? || environment?(:test) ? '' : ' --watch'}",
           source: "tmp/frontend",
           latency: 3
end

# Begin proxy resources
org_chart = OrgChart.new
proxy '/company/team/org-chart/index.html', '/company/team/org-chart/template.html', locals: { team_data_tree: org_chart.team_data_tree }, ignore: true

# Proxy Comparison html and PDF pages
data.features.devops_tools.each_key do |devops_tool|
  next if devops_tool[0..6] == 'gitlab_'

  file_name = "#{devops_tool}-vs-gitlab".tr('_', '-')
  proxy "/devops-tools/#{file_name}.html", "/templates/comparison.html", locals: {
    key_one: devops_tool,
    key_two: 'gitlab_ultimate'
  }

  proxy "/devops-tools/pdfs/#{file_name}.html", '/devops-tools/pdfs/template.html', locals: {
    key_one: devops_tool,
    key_two: 'gitlab_ultimate'
  }, ignore: true
end

data.features_v2.devops_tools.each_key do |devops_tool|
  next if devops_tool[0..6] == 'gitlab_'

  file_name = "#{devops_tool}-vs-gitlab".tr('_', '-')
  proxy "/devops-tools/#{file_name}.html", "/templates/comparison-v2.html", locals: {
    key_one: devops_tool,
    key_two: 'gitlab_ultimate'
  }

  proxy "/devops-tools/pdfs/#{file_name}.html", '/devops-tools/pdfs/template.html', locals: {
    key_one: devops_tool,
    key_two: 'gitlab_ultimate'
  }, ignore: true
end

# Analyst reports
data.analyst_reports.each do |report|
  next unless report.url

  proxy "/analysts/#{report.url}/index.html", '/analysts/template.html', locals: {
    report: report
  }, ignore: true
end

# Category pages for /stages-devops-lifecycle
data.categories.each do |key, category|
  next unless category.body && category.maturity && (category.maturity != "planned") && category.marketing

  proxy "/stages-devops-lifecycle/#{key.dup.tr('_', '-').downcase}/index.html", '/stages-devops-lifecycle/template.html', locals: {
    category: category,
    category_key: key
  }, ignore: true
end

# Event pages
data.events.each do |event|
  next unless event.url

  proxy "/events/#{event.url.tr(' ', '-')}/index.html", '/events/template.html', locals: {
    event: event
  }, ignore: true
end

# Conversion pages under /forms
data.forms.each do |form|
  proxy "/forms/#{form.url.tr(' ', '-')}/index.html", '/forms/template.html', locals: {
    form: form
  }, ignore: true
end

# Webcast pages
data.webcasts.each do |webcast|
  proxy "/webcast/#{webcast.url.tr(' ', '-')}/index.html", '/webcast/template.html', locals: {
    webcast: webcast
  }, ignore: true
end

# Reseller page
data.resellers.each do |reseller|
  proxy "/resellers/#{reseller.name.mb_chars.normalize(:kd).gsub(/[^\x00-\x7F]/n, '').downcase.to_s.tr(' ', '-')}/index.html", '/resellers/template.html', locals: {
    reseller: reseller
  }, ignore: true
end

# Release Radars /webcast/monthly-release
data.release_radars.each do |release_radar|
  proxy "/webcast/monthly-release/#{release_radar.name.tr(' ', '-').downcase}/index.html", '/webcast/monthly-release/template.html', locals: {
    release_radar: release_radar
  }, ignore: true
end

# create new job listing instance when /jobs/apply reached
proxy "/jobs/careers/index.html", '/jobs/apply/template.html', ignore: true

# Proxy case study pages
if @app.data.respond_to?(:case_studies)
  data.case_studies.each do |filename, study|
    proxy "/customers/#{filename}/index.html", '/templates/case_study.html', locals: { case_study: study }
  end
end

# See https://gitlab.com/gitlab-com/infrastructure/issues/4036
proxy '/development/index.html', '/sales/index.html'
# End proxy resources

#----------------------------------------------------------
# End global config (not specific to development or build mode)
#----------------------------------------------------------

# Development-specific config
configure :development do
  # Reload the browser automatically whenever files change
  activate :livereload unless ENV['ENABLE_LIVERELOAD'] != '1'

  #-------------------------------------------------------------------
  # Begin development-specific monorepo config (shared from sub-sites)
  #-------------------------------------------------------------------

  monorepo_sites = Psych.load_file(File.join(Middleman::Application.root, 'data/monorepo.yml')).keys
  monorepo_sites.delete('blog') # TODO: Skip blog for now because the files are still at the top level, remove this line once the files are actually moved and it is converted to the new non-symlink approach
  monorepo_sites.each do |site|
    # In development mode, top-level build will build all files, including all
    # files from monorepo sub-sites, so we need to have a files.watch for all
    # their source directories.
    site_dir = File.expand_path("sites/#{site}", __dir__)
    files.watch(
      :source,
      path: "#{site_dir}/source"
    )

    # Include all the proxy resource configs for the sub-sites
    config_proxy_resources = File.join(site_dir, 'config_proxy_resources.rb')
    instance_eval(File.read(config_proxy_resources), config_proxy_resources, 1)
  end

  #-------------------------------------------------------------------
  # End development-specific monorepo config (shared from sub-sites)
  #-------------------------------------------------------------------
end

# Build-specific config
configure :build do
  Kramdown::Converter::PlantUmlHtmlWrapper.plantuml_setup
  set :build_dir, 'public'

  activate :minify_css
  activate :minify_javascript
  # Mermaid diagrams don't render without line breaks
  activate :minify_html, preserve_line_breaks: true

  # Begin proxy resources

  # create routes to job page using job-listing template

  # Currently commenting out the iteration below on jobs
  # When ready to repost jobs to the jobs page please comment
  # back in lines below

  # Gitlab::Homepage::Jobs::JobsListing.new.jobs.each do |job|
  #   proxy "/jobs/apply/#{job[:title].downcase.strip.tr(' ', '-').gsub(/[^\w-]/, '')}-#{job[:id]}/index.html",
  #         "/jobs/apply/job-listing/template.html",
  #         locals: { job: job },
  #         ignore: true
  # end

  # Populate the direction and releases pages only on master or development machines.
  # That will help shave off some time of the build times on branches.
  if ENV['INCLUDE_GENERATORS'] == 'true' || !ENV.key?('CI_SERVER')

    # Direction page
    if ENV['PRIVATE_TOKEN']
      content = {}
      direction = Generators::Direction.new

      wishlist = direction.generate_wishlist # wishlist, shared by most pages
      direction_all_content = direction.generate_direction(Generators::Direction::STAGES) # /direction/*
      direction_dev_content = direction.generate_direction(Generators::Direction::DEV_STAGES) # /direction/dev/
      direction_ops_content = direction.generate_direction(Generators::Direction::OPS_STAGES) # /direction/ops/
      direction_enablement_content = direction.generate_direction(Generators::Direction::ENABLEMENT_STAGES) # /direction/ops/

      Generators::Direction::STAGES.each do |name|
        # Fetch content for per-team pages
        skip = %w[release verify package] # these do not have a team page
        content[name] = direction.generate_direction(%W[#{name}]) unless skip.include? name # /direction/name/
      end

      stage_contribution_content = direction.generate_contribution_count(data.stages) # /direction/maturity/
      stage_velocity = direction.generate_stage_velocity(data.stages) # /direction/maturity/

      milestones = direction.generate_milestones # /releases/gitlab-com

      # Set up proxies using now-fetched content for shared pages
      proxy '/upcoming-releases/index.html', '/upcoming-releases/template.html', locals: { direction: direction_all_content }, ignore: true
      proxy '/direction/paid_tiers/index.html', '/direction/paid_tiers/template.html', locals: { wishlist: wishlist }, ignore: true
      proxy '/releases/gitlab-com/index.html', '/releases/gitlab-com/template.html', locals: { milestones: milestones }, ignore: true
      proxy '/direction/dev/index.html', '/direction/dev/template.html', locals: { direction: direction_dev_content }, ignore: true
      proxy '/direction/ops/index.html', '/direction/ops/template.html', locals: { direction: direction_ops_content }, ignore: true
      proxy '/direction/enablement/index.html', '/direction/enablement/template.html', locals: { direction: direction_enablement_content }, ignore: true
      proxy '/direction/maturity/index.html', '/direction/maturity/template.html', locals: { stage_contributions: stage_contribution_content, stage_velocity: stage_velocity }, ignore: true
      proxy '/direction/kickoff/index.html', '/direction/kickoff/template.html', locals: { direction: direction_all_content }, ignore: true
      proxy '/direction/moonshots/index.html', '/direction/moonshots/template.html', locals: { wishlist: wishlist }, ignore: true

      Generators::Direction::STAGES.each do |name|
        # And for team pages
        skip = %w[release verify package] # these do not have a team page
        proxy "/direction/#{name}/index.html", "/direction/#{name}/template.html", locals: { direction: content[name], wishlist: wishlist }, ignore: true unless skip.include? name
      end
    end

    ## Releases page
    releases = ReleaseList.new
    proxy '/releases/index.html', '/releases/template.html', locals: {
      list: releases.generate,
      count: releases.count
    }, ignore: true
  end
  # End proxy resources
end
